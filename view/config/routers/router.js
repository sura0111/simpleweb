const page = (path) => {
    return require( '../../pages/' + path )
}
module.exports = ($stateProvider, $locationProvider) => {
    $stateProvider
    .state('web', {
        url: '',
        abstract: true,
        template: page(`index.html`)
    })
    .state('web.home', {
        url: '/',
        template: page(`home/home.html`)
    })
    .state('web.test', {
        url: '/test',
        template: page(`test/test.html`)
    })
}


