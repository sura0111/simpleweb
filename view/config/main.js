// Copyright by Sura
// 2017.08.06

// Bootstrap
import 'bootstrap/dist/js/bootstrap';
require('bootstrap/dist/css/bootstrap.css')

require('./styles/style.less')

// Main angular module
window.App = require('./lib/angular')

// Markdown
window.marked = require('./lib/marked')


// Angular default configuration
const app = App.main();

app.config(($controllerProvider, $compileProvider, $filterProvider, $provide, $stateProvider, $urlRouterProvider, $locationProvider) => {
  app.register = {
    controller: $controllerProvider.register,
    directive: $compileProvider.directive,
    filter: $filterProvider.register,
    factory: $provide.factory,
    service: $provide.service
  }
  $stateProvider.state('otherwise', { url: '/404', templateUrl: '/404.html' })
  $urlRouterProvider.otherwise('/404');
  $locationProvider.html5Mode(true);
})

App.add('route').config(require('./routers/router'))
