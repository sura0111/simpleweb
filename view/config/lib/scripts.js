var loadScript = function (url, type, charset) {
    return new Promise((resolve, reject) => {
        if (url) {
            if (type === undefined) type = 'text/javascript';
            if (charset === undefined) charset = 'utf-8';
            var script = document.querySelector("script[src*='" + url + "']");
            if (!script) {
                script = document.createElement('script');
                script.setAttribute('src', url);
                script.setAttribute('type', type);
                if (charset) script.setAttribute('charset', charset);
                document.body.appendChild(script);
            };
            script.onload = () => resolve(script);
            script.onerror = () => reject(script);
            return;
        }
        reject(script);
    })
};

var loadScripts = (files) => {
    var loaded = -1;
    var count = files.length;
    return new Promise((resolve, reject) => {
        var next = () => {
            if (loaded++ < count - 1) {
                return loadScript(files[loaded]).then(next, next)
            }
            return resolve()
        }
        next()
    })
}

module.exports = {
    load: loadScripts
};