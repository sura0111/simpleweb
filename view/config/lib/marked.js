var hljs = require('highlight.js')
require('highlight.js/styles/atom-one-light.css')
var marked = require('marked')

var renderer = new marked.Renderer()

renderer.code = (code, language) => {
    return '<pre' + '><code class="hljs">' + hljs.highlightAuto(code).value + '</code></pre>';
}

marked.setOptions({
    renderer: renderer
})

module.exports = marked;
