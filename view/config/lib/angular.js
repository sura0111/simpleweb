window.angular = require("angular")
require('@uirouter/angularjs')
require('angular-sanitize')
require('angular-animate')
require('angular-resource')
require('angular-cookies')

var app = {
  name: 'app',
  modules: ['ui.router', 'ngSanitize', 'ngResource', 'ngCookies'],

  main: function () {
    return angular.module(this.name);
  },

  add: function (moduleName, dependencies) {
    angular.module(moduleName, dependencies || [])
    angular.module(this.name).requires.push(moduleName);
    return angular.module(moduleName)
  }

};

app.add(app.name, app.modules)

module.exports = app;