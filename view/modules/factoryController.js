var factories = ['imageResize']

factories.forEach(function (factory) {
    App.main().factory(factory, require(`./factories/${factory}`))
}, this);