module.exports = ($q) => {
    var imageResize;
    imageResize = function (img, size) {
        var canvas, ctx, h, temp, w;
        w = img.width;
        h = img.height;
        canvas = document.createElement('canvas');
        ctx = canvas.getContext('2d');
        temp = Math.sqrt(img.width * img.width + img.height * img.height);
        if (temp > size) {
            w = size * img.width / temp;
            h = size * img.height / temp;
        }
        canvas.width = w;
        canvas.height = h;
        ctx.drawImage(img, 0, 0, w, h);
        return canvas.toDataURL();
    };
    return imageResize;
}