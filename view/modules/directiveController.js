var directives = ['dropimage', 'dropdown', 'scrolls', 'marked', 'spacer', 'upimage']

directives.forEach(function(directive) {
    App.main().directive(directive, require(`./directives/${directive}`))
}, this);