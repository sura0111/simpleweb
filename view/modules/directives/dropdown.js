/*
Sura,Kh. 

How to use
-----------------------------------------
<dropdown>
    <div dropbutton class="dropbutton">
        ...
    </div>
    <div dropmenu class="dropmenu">
        ...
    </div>
</dropdown>
-----------------------------------------

*/

module.exports = ()=>{
    return {
        restrict: 'E',
        template: '<ng-transclude></ng-transclude>',
        transclude: true,
        link: (s,e,a)=>{
            var show = 0; 
            var wait = false;
            var showtypes = ['none', 'block'];
            var button = e[0].querySelector('[dropbutton]'); 
            button.tabIndex = 0;
            var menu = e[0].querySelector('[dropmenu]');
            e[0].style.display = 'inline-block';
            
            var checkWidth = ()=>{   
                menu.style.minWidth = button.offsetWidth;
                if(menu.offsetLeft+menu.offsetWidth>=window.innerWidth) menu.style.right='0px';        
                else menu.style.right = ''
            }      
            var set = (flag) =>{
                if(flag == undefined){ 
                    show = 1-show 
                } else {
                    show = flag;
                }
                menu.style.display = showtypes[show]; 
            }
            set(0)

            button.addEventListener('click',      ()=> set()              )
            button.addEventListener('blur',       ()=>{ if(!wait) set(0) })
            menu.addEventListener  ('click',      ()=> set(0)             )
            menu.addEventListener  ('mouseenter', ()=> wait = true        )
            menu.addEventListener  ('mouseleave', ()=> wait = false       )
        }
    }
}