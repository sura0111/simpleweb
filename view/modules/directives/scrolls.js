module.exports = ($window, $timeout)=>{
  return {
    restrict: 'E',
    template: `
    <div class="pagetop" ng-click="goToTop()" ng-hide='hide'>
      < i class="fa fa-chevron-up fa-2x" ></i>
    </div>
    `,
    link: (s,e,a) => {
      s.hide = false
      s.goToTop = ()=> {
        $("body").animate({ scrollTop: 0 }, "slow");
      }
      var timeout = null;
      var onScrollFinished = ()=>{
        s.hide = true
        e.find('div').addClass('shidden')
      }
      $window.onscroll = (event)=>{
        var windowHeight = window.innerHeight;
        var scrollPosition = document.body.scrollTop;
        if(scrollPosition>windowHeight){
          s.hide = false
        } else {
          s.hide = true
        }
        $timeout.cancel(timeout)
        timeout = $timeout(onScrollFinished, 2000)
      }
    }
  }
}
// .directive('scrollOnClick', function () {
//   return {
//     restrict: 'A',
//     link: function (scope, $elm) {
//       $elm.on('click', function () {
//         $("body").animate({ scrollTop: $elm.offset().top }, "slow");
//       });
//     }
//   }
// });