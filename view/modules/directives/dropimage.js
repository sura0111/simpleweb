module.exports = ($q, imageResize, $compile) => {
    return {
        restrict: 'A',
        scope: { process: '=?', dropimage: '&' },
        link: function (scope, elm, attr) {
            var make;
            scope.process = false;
            elm.addClass('dropimage');
            var dropImage = (image) => {
                scope.process = false;
                scope.dropimage({ data: image })
            }
            var loadImage = (flag) => {
                scope.$apply(() => scope.process = flag != undefined ? flag : true);
            }
            elm[0].addEventListener('dragover', function (event) {
                event.preventDefault();
                event.dataTransfer.dropEffect = 'copy';
                return elm.addClass('dragover');
            });
            elm[0].addEventListener('dragleave', (event) => elm.removeClass('dragover'));
            elm[0].addEventListener('drop', function (event) {
                event.preventDefault();
                loadImage()
                elm.removeClass('dragover');
                var file = event.dataTransfer.files[0];
                if (file) {
                    if (file.type.indexOf('image') !== 0) { return loadImage(false); }
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        var src = event.target.result;
                        var image = new Image();
                        image.src = src;
                        image.onload = () => dropImage(imageResize(image, 1500));
                    };
                    reader.readAsDataURL(file);
                } else {
                    var droppedHTML = event.dataTransfer.getData('text/html');
                    var dropContext = $('<div>').append(droppedHTML);
                    var src = $(dropContext).find("img").attr('src')
                    loadImage()
                    $('<img/>').attr('src', src).on('load', () => dropImage(src));
                }
            });
        }
    }
}
