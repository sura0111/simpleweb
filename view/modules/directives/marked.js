module.exports = ($sce) => {
    return {
        restrict: 'E',
        template: `<div ng-bind-html="data()"></div>`,
        scope: { ngModel: '=' },
        link: (s, e, a) => {
            s.data = () => { return marked(s.ngModel) };
        }
    }
}