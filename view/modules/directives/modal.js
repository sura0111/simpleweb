module.exports = ($timeout)=>{
  return {
    restrict: 'E',
    transclude: true,
    scope: {name:'=', class:'@ngClass'},
    template: `
    <dim ng-show="showing" ng-click="hide()"></dim>
    <modalmenu class="{{class}}" ng-click="show()" ng-bind-html="name"></modalmenu>
    <modalbody ng-show="showing" class="modalbody animate-show-hide">
      <modalclose ng-click="hide()"><i class="fa fa-times"></i></modalclose>
      <div ng-transclude style="margin-top:20px;"></div>
    </modalbody>
    `,
    link: (scope, elm, attr)=>{
      angular.element(()=>{
        var mbody = elm[0].querySelector("modalbody");
        var mmenu = elm[0].querySelector("modalmenu");
        var mclose = elm[0].querySelector("modalclose");
        scope.showing = false;
        scope.show=()=>{ scope.showing = true; }
        scope.hide=()=>{ scope.showing=false; }
        $timeout(()=>{
          elm[0].querySelectorAll('[modalclose]').forEach(function(element) {
            var click = (event)=>{ scope.showing = false; scope.$apply() }
            element.addEventListener('click', click);
          }, this);
        })
      })
    }
  }
}