module.exports = ()=>{
  return{
    restrict: 'E',
    template: '<div/>',
    scope: {height:'='},
    link: (scope, elm, attr)=>{
      elm[0].querySelector('div').style.height = scope.height + 'px';
    }
  }
}