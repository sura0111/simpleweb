module.exports = ($q, imageResize, $compile, $timeout)=> {
  return {
    restrict: 'A',
    transclude: true,
    scope: {process:'=?', upimage:'&'},
    template: `
  <ng-transclude></ng-transclude>
  <input type="file" style="display:none;">
  `,
    link: function (scope, elm, attr) {
      scope.process = false;
      var upImage = (image)=>{
        scope.process = false;
        scope.upimage({data:image})
      }
      var loadImage = (flag)=>{
        $timeout(()=>{
          scope.process = flag? true : false
        });
      }
      elm[0].addEventListener('click', ()=> elm[0].querySelector('input[type="file"]').click())
      elm[0].querySelector('input[type="file"]').addEventListener('change', function (changeEvent) {
        loadImage(true)
        var reader = new FileReader();
        reader.onload = function (event) {
          var src = event.target.result;
          if (!src.match(/^data\:(.*?)\//)) return loadImage(false);
          if (src.match(/^data\:(.*?)\//)[1] !== 'image') return loadImage(false);
          var image = new Image();
          image.src = src;
          image.onload = ()=>upImage(imageResize(image, 1500));
          loadImage(false)
        };
        reader.readAsDataURL(changeEvent.target.files[0]);
        elm[0].querySelector('input[type="file"]').value = '';
      });
    }
  };
}