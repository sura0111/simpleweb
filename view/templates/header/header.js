App.main()
.directive('header', ()=>{
  return {
    restrict: 'E',
    template: require('./header.html')
  }
})