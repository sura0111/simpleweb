App.main().directive('footer', ($transitions, $timeout) => {
    return {
        restrict: 'E',
        template: require('./footer.html'),
        link: (scope, elm, attr) => {
            var spacer = document.createElement('div')
            elm[0].parentElement.insertBefore(spacer, elm[0])
            var footer = () => {
                spacer.style.height = ''
                var diff = window.innerHeight - elm[0].offsetTop
                if (diff > 0) {
                    spacer.style.height = diff + "px";
                }
            }
            footer();
            window.addEventListener('resize', () => { 
                footer() 
            })
            $transitions.onFinish({}, (trans) => {
                $timeout(footer)
            })
        }
    }
})