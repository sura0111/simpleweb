const path = require('path')

process.env.dirname = __dirname
process.env.MCP = path.join(__dirname, 'config', 'back', 'MC')

const BConfig = require(path.join(__dirname, 'config', 'back', 'CBConfig'))
const App = require('./config/CConfig')
const app = App()

app.use('/api', BConfig.router)
app.use('/view', require('./config/front/CFConfig'))


app.get('/404', (req, res) => {
  res.sendFile(path.join(__dirname, 'view/error.html'))
})

app.get('/*', (req, res) => {
  if (!req.user) {
    for (var name in req.cookies) {
      if (req.cookies.hasOwnProperty(name)) {
        res.clearCookie(name)
      }
    }
  }
  res.sendFile(path.join(__dirname, 'view/index.html'))
})

// SERVER START --------------------------------------------------------------------------
server = app.listen(3000, '0.0.0.0', () => {
  console.log("----------------------------------------------");
  console.log("Server is working on: http://" + server.address().address + ":" + server.address().port);
  console.log("----------------------------------------------");
});