![screenshot](./static/images/Screenshot.png)

```bash
.
├── README.md
├── app.js
├── back
│   ├── config
│   ├── main.js
│   └── sub
├── config
│   ├── appConfig.js
│   ├── backConfig.js
│   └── viewConfig.js
├── gulpfile.js
├── node_modules...
├── package.json
├── static
│   ├── favicons
│   ├── images
│   ├── js
│   └── scripts
├── view
│   ├── 404.html
│   ├── config
│   ├── index.html
│   ├── modules
│   ├── pages
│   └── templates
└── webpack.config.js
```