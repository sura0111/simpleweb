var gulp = require('gulp');
var watch = require('gulp-watch');
var webpack = require('webpack-stream');
var gls = require('gulp-live-server');

var path = require('path')
var chalk = require('chalk')
var glob = require('glob')

var notification = (event) => { console.log('File ' + event.path + ' was ' + event.type); }

gulp.task('server', function () {
    var server = gls.new('app.js');
    server.start();
    watch(['app.js', 'back/**/*.js', 'config/*.js'], (file) => { })
    .on('change', (event) => {
        server.start.bind(server)()
        console.log(chalk.green('changed:'), event)
    })
    .on('add', (event) => {
        server.start.bind(server)();
        console.log(chalk.yellow('added'), event)
    })
    .on('unlink', (event) => {
        server.start.bind(server)();
        console.log(chalk.red('deleted'), event)
    });
    
    watch(['static', 'view/**/*.html'], (file) => {
        server.notify.apply(server, [file]);
    })
    .on('change', (event) => {
        console.log(chalk.green('changed: ') + event)
    })
    .on('add', (event) => {
        server.start.bind(server)();
        console.log(chalk.yellow('added: ') + event)
    })
    .on('unlink', (event) => {
        server.start.bind(server)();
        console.log(chalk.red('deleted: ') + event)
    })
});

gulp.task('bundle', function () {
    gulp.src('view/**/*')
    .pipe(webpack(require('./webpack.config')))
    .pipe(gulp.dest('static/scripts/'));
    
});

var services = ['server', 'bundle']

gulp.task('default', services, () => { });
