const webpack = require("webpack")
const glob = require('glob');
const fs = require('fs');
const path = require('path');

const CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin;

const entry = () => {
    var paths = {};
    var viewPath = path.join(__dirname, 'view', '/**/*.js')
    var configPath = path.join(__dirname, 'view', 'config', '/**/*.js')
    var views = glob.sync(viewPath, {ignore: configPath})
    var config = glob.sync(configPath)
    if(config.length){
        paths.config = config
    }
    if(views.length){
        paths.views = views
    }
    return paths
}

module.exports = {
    entry: entry(),
    output: {
        path: path.join(__dirname, "static/scripts"),
        filename: "[name].bundle.js"
    },
    watch: true,
    module: {
        rules: [

            // Javascript loader
            {
                test: /\.jsx?$/,
                exclude: ['node_modules'],
                loader: "babel-loader",
                options: {
                    presets: ['react', "env"], compact: true
                }
            },

            // Less loader
            {
                test: /\.less$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                    { loader: "less-loader" }
                ]
            },

            // Sass, Scss loader
            { test: /\.scss$/, loaders: ['style', 'css', 'sass']},

            // Image loader
            { test: /\.(jpg|png|gif)$/, loaders: 'url-loader' },

            // Text loader
            { test: /\.txt$/, loader: 'raw-loader' },

            // Html loader
            { test: /\.html$/, loader: 'html-loader' },

            { test: /\.css$/, loader: 'style-loader!css-loader' },
            { test: /\.svg$/, loader: 'url-loader?mimetype=image/svg+xml' },
            { test: /\.woff$/, loader: 'url-loader?mimetype=application/font-woff' },
            { test: /\.woff2$/, loader: 'url-loader?mimetype=application/font-woff' },
            { test: /\.eot$/, loader: 'url-loader?mimetype=application/font-woff' },
            { test: /\.ttf$/, loader: 'url-loader?mimetype=application/font-woff' }
        ]
    },
    
    plugins: [
        new CommonsChunkPlugin({ 
            name: 'common', 
            filename: 'common.bundle.js' }),
            
            new webpack.ProvidePlugin({
                jQuery: 'jquery', 
                $: 'jquery', 
                jquery: 'jquery'
            })
        ]
        
    }
    