const glob = require('glob');
const path = require('path');
const express = require('express')
const router = express.Router()

const files = glob.sync(path.join(process.env.dirname, 'view/**/*.html'))
.map((value) => {
    return {
        url: value.replace(path.join(process.env.dirname, 'view'), ''),
        path: value
    }
})

files.forEach((file)=>{
    router.get(file.url, (req, res) => {
        res.sendFile(file.path)
    })
}, this);

module.exports = router;