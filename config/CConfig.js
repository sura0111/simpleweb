module.exports = () => {
    const Log4js = require('log4js');
    const logger = Log4js.getLogger();
    const favicon = require('serve-favicon')
    const path = require('path')

    const mongoose = require('mongoose')
    const passport = require('passport')
    const LocalStrategy = require('passport-local').Strategy;

    // logging settings
    // Log4js.configure({
    //   appenders: { accesslog: { type: 'file', filename: 'accesslog.log' } },
    //   categories: { default: { appenders: ['accesslog'], level: 'debug' } }
    // });
    logger.level = 'debug'

    const express = require('express');
    const cookieParser = require('cookie-parser')
    const session = require('express-session');
    const bodyParser = require('body-parser');

    var app = express();
    app.use( bodyParser.urlencoded({ extended: true }) );
    app.use( bodyParser.json({ limit: '30mb' }) );

    app.use( session({ secret: 'keyboard cat', resave: true, saveUninitialized: true }) );
    app.use( cookieParser() )

    // mongoose.Promise = global.Promise;

    // mongoose.connect('mongodb://localhost/mgexpress', { useMongoClient: true }).then(() => {
    //     console.log('MONGODB CONNECTION SUCCESFULL');
    // }, (err) => {
    //     console.log('connection error:', err)
    // });
    
    app.use(passport.initialize());
    app.use(passport.session());

    // AuthConfiguration = () => {
    //     var User = mongoose.model('User');
    //     passport.serializeUser(function (user, done) { done(null, user); });
    //     passport.deserializeUser(function (user, done) { done(null, user); });
    //     passport.use(new LocalStrategy({ usernameField: 'username', passwordField: 'password' },
    //         function (username, password, done) {
    //             User.findOne({ username: username.toLowerCase() }, function (err, user) {
    //                 if (err) { return done(err); }
    //                 if (!user) {
    //                     return done(null, false, { message: 'Incorrect username.' });
    //                 }
    //                 if (!user.authenticate(password)) {
    //                     return done(null, false, { message: 'Incorrect password.' });
    //                 }
    //                 return done(null, user);
    //             });
    //         }
    //     ));
    // }
    // AuthConfiguration()

    app.use((req, res, next) => {
        logger.debug(req.originalUrl)
        next()
    })

    app.use(favicon(path.join(process.env.dirname, 'static', 'favicons', 'favicon.ico')));

    app.use('/static', express.static('static'));
    
    return app
}