module.exports = ()=>{
    const glob = require('glob');
    const path = require('path');
    const express = require('express')
    const router = express.Router()

    const files = glob.sync(path.join(process.env.dirname, 'back/routers/**/*.js'))
        .map((value) => {
            return {
                url: value.replace(path.join(process.env.dirname, 'back/routers'), '').slice(0, -3),
                path: value
            }
        })

    files.forEach((file) => {
        console.log(file.url, file.path);
        router.use(file.url, require(file.path))
    }, this);

    return router
};