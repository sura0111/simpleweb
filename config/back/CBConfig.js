const Model = require('./CBModel')
const Controller = require('./CBController')
const Permission = require('./CBPermission')
const Router = require('./CBRouter')

Model.init()
Controller.init()
Permission.init()
const router = Router()
module.exports = {
    router: router
}