const Model = require('./CBModel')
const Controller = require('./CBController')
const Permission = require('./CBPermission')
const MC = {
    model: Model.model,
    controller: Controller.controller,
    permission: Permission.permission
}

module.exports = MC