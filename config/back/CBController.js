const path = require('path')
const glob = require('glob')

let controllers = {}

module.exports = {
    init: () => {
        const files = glob.sync(path.join(process.env.dirname, 'back/controllers/**/*.js')).map((value) => {
            return {
                seps: value.replace(path.join(process.env.dirname, 'back/controllers/'), '').slice(0, -3).split('/'),
                path: value
            }
        })
        files.forEach((file) => {
            controllers[file.seps[file.seps.length - 1]] = require(file.path)
        }, this);
    },
    controller: (name, ctrl) => {
        if (ctrl) {
            if (controllers.hasOwnProperty(name)) {
                throw `ERROR setting controller: '${name}' already exists`
            }
            controllers[name] = ctrl
        } else {
            if (!controllers.hasOwnProperty(name)) {
                console.log(`ERROR getting controller: '${name}' is not exist`);
                return
            }
        }
        return controllers[name]
    }
}