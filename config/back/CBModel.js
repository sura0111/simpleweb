const mongoose = require('mongoose')
const glob = require('glob')
const path = require('path')

module.exports = {
    init: () => {
        var files = glob.sync(path.join(process.env.dirname, '/back/models/**/*.js'))
        files.forEach(function (file) {
            require(file)
        }, this);
    },
    model: (name) => {
        return mongoose.model(name)
    }
}
    
