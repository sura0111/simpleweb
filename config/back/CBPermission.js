const path = require('path')
const glob = require('glob')

let permissions = {}

module.exports = {
    init: () => {
        const files = glob.sync(path.join(process.env.dirname, 'back/permissions/**/*.js')).map((value) => {
            return {
                seps: value.replace(path.join(process.env.dirname, 'back/permissions/'), '').slice(0, -3).split('/'),
                path: value
            }
        })
        files.forEach((file) => {
            permissions[file.seps[file.seps.length - 1]] = require(file.path)
        }, this);
    },
    permission: (name, pms) => {
        if (pms) {
            if (permissions.hasOwnProperty(name)) {
                throw `ERROR setting permission: '${name}' already exists`
            }
            permissions[name] = pms
            return permissions[name]
        } else {
            var roles = [];
            roles = roles.concat(name);
            for (var role of roles) {
                if (!permissions.hasOwnProperty(role)) {
                    throw `ERROR getting permission: '${role}' is not exist`;
                }
                return permissions[role]
            }
        }
    }
}