webpackJsonp([1],{

/***/ 300:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(301);
__webpack_require__(31);
__webpack_require__(32);
__webpack_require__(33);
__webpack_require__(34);
__webpack_require__(35);
__webpack_require__(36);
__webpack_require__(37);
__webpack_require__(38);
__webpack_require__(303);
__webpack_require__(30);
__webpack_require__(305);
module.exports = __webpack_require__(307);


/***/ }),

/***/ 301:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var directives=['dropimage','dropdown','scrolls','marked','spacer','upimage'];directives.forEach(function(directive){App.main().directive(directive,__webpack_require__(302)("./"+directive));},undefined);

/***/ }),

/***/ 302:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./dropdown": 31,
	"./dropdown.js": 31,
	"./dropimage": 32,
	"./dropimage.js": 32,
	"./marked": 33,
	"./marked.js": 33,
	"./modal": 34,
	"./modal.js": 34,
	"./scrolls": 35,
	"./scrolls.js": 35,
	"./spacer": 36,
	"./spacer.js": 36,
	"./upimage": 37,
	"./upimage.js": 37
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 302;

/***/ }),

/***/ 303:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var factories=['imageResize'];factories.forEach(function(factory){App.main().factory(factory,__webpack_require__(304)("./"+factory));},undefined);

/***/ }),

/***/ 304:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./imageResize": 38,
	"./imageResize.js": 38
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 304;

/***/ }),

/***/ 305:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
App.main().directive('footer',function($transitions,$timeout){return{restrict:'E',template:__webpack_require__(306),link:function link(scope,elm,attr){var spacer=document.createElement('div');elm[0].parentElement.insertBefore(spacer,elm[0]);var footer=function footer(){spacer.style.height='';var diff=window.innerHeight-elm[0].offsetTop;if(diff>0){spacer.style.height=diff+"px";}};footer();window.addEventListener('resize',function(){footer();});$transitions.onFinish({},function(trans){$timeout(footer);});}};});

/***/ }),

/***/ 306:
/***/ (function(module, exports) {

module.exports = "<div>Feel free to use.</div>";

/***/ }),

/***/ 307:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
App.main().directive('header',function(){return{restrict:'E',template:__webpack_require__(308)};});

/***/ }),

/***/ 308:
/***/ (function(module, exports) {

module.exports = "<div>\n    <a ui-sref=\"web.home({reload:true})\">Sample Node Application</a>\n</div>";

/***/ }),

/***/ 31:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*
Sura,Kh. 

How to use
-----------------------------------------
<dropdown>
    <div dropbutton class="dropbutton">
        ...
    </div>
    <div dropmenu class="dropmenu">
        ...
    </div>
</dropdown>
-----------------------------------------

*/module.exports=function(){return{restrict:'E',template:'<ng-transclude></ng-transclude>',transclude:true,link:function link(s,e,a){var show=0;var wait=false;var showtypes=['none','block'];var button=e[0].querySelector('[dropbutton]');button.tabIndex=0;var menu=e[0].querySelector('[dropmenu]');e[0].style.display='inline-block';var checkWidth=function checkWidth(){menu.style.minWidth=button.offsetWidth;if(menu.offsetLeft+menu.offsetWidth>=window.innerWidth)menu.style.right='0px';else menu.style.right='';};var set=function set(flag){if(flag==undefined){show=1-show;}else{show=flag;}menu.style.display=showtypes[show];};set(0);button.addEventListener('click',function(){return set();});button.addEventListener('blur',function(){if(!wait)set(0);});menu.addEventListener('click',function(){return set(0);});menu.addEventListener('mouseenter',function(){return wait=true;});menu.addEventListener('mouseleave',function(){return wait=false;});}};};

/***/ }),

/***/ 32:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {module.exports=function($q,imageResize,$compile){return{restrict:'A',scope:{process:'=?',dropimage:'&'},link:function link(scope,elm,attr){var make;scope.process=false;elm.addClass('dropimage');var dropImage=function dropImage(image){scope.process=false;scope.dropimage({data:image});};var loadImage=function loadImage(flag){scope.$apply(function(){return scope.process=flag!=undefined?flag:true;});};elm[0].addEventListener('dragover',function(event){event.preventDefault();event.dataTransfer.dropEffect='copy';return elm.addClass('dragover');});elm[0].addEventListener('dragleave',function(event){return elm.removeClass('dragover');});elm[0].addEventListener('drop',function(event){event.preventDefault();loadImage();elm.removeClass('dragover');var file=event.dataTransfer.files[0];if(file){if(file.type.indexOf('image')!==0){return loadImage(false);}var reader=new FileReader();reader.onload=function(event){var src=event.target.result;var image=new Image();image.src=src;image.onload=function(){return dropImage(imageResize(image,1500));};};reader.readAsDataURL(file);}else{var droppedHTML=event.dataTransfer.getData('text/html');var dropContext=$('<div>').append(droppedHTML);var src=$(dropContext).find("img").attr('src');loadImage();$('<img/>').attr('src',src).on('load',function(){return dropImage(src);});}});}};};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(29)))

/***/ }),

/***/ 33:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
module.exports=function($sce){return{restrict:'E',template:'<div ng-bind-html="data()"></div>',scope:{ngModel:'='},link:function link(s,e,a){s.data=function(){return marked(s.ngModel);};}};};

/***/ }),

/***/ 34:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
module.exports=function($timeout){return{restrict:'E',transclude:true,scope:{name:'=',class:'@ngClass'},template:'\n    <dim ng-show="showing" ng-click="hide()"></dim>\n    <modalmenu class="{{class}}" ng-click="show()" ng-bind-html="name"></modalmenu>\n    <modalbody ng-show="showing" class="modalbody animate-show-hide">\n      <modalclose ng-click="hide()"><i class="fa fa-times"></i></modalclose>\n      <div ng-transclude style="margin-top:20px;"></div>\n    </modalbody>\n    ',link:function link(scope,elm,attr){angular.element(function(){var mbody=elm[0].querySelector("modalbody");var mmenu=elm[0].querySelector("modalmenu");var mclose=elm[0].querySelector("modalclose");scope.showing=false;scope.show=function(){scope.showing=true;};scope.hide=function(){scope.showing=false;};$timeout(function(){elm[0].querySelectorAll('[modalclose]').forEach(function(element){var click=function click(event){scope.showing=false;scope.$apply();};element.addEventListener('click',click);},undefined);});});}};};

/***/ }),

/***/ 35:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {module.exports=function($window,$timeout){return{restrict:'E',template:"\n    <div class=\"pagetop\" ng-click=\"goToTop()\" ng-hide='hide'>\n      < i class=\"fa fa-chevron-up fa-2x\" ></i>\n    </div>\n    ",link:function link(s,e,a){s.hide=false;s.goToTop=function(){$("body").animate({scrollTop:0},"slow");};var timeout=null;var onScrollFinished=function onScrollFinished(){s.hide=true;e.find('div').addClass('shidden');};$window.onscroll=function(event){var windowHeight=window.innerHeight;var scrollPosition=document.body.scrollTop;if(scrollPosition>windowHeight){s.hide=false;}else{s.hide=true;}$timeout.cancel(timeout);timeout=$timeout(onScrollFinished,2000);};}};};// .directive('scrollOnClick', function () {
//   return {
//     restrict: 'A',
//     link: function (scope, $elm) {
//       $elm.on('click', function () {
//         $("body").animate({ scrollTop: $elm.offset().top }, "slow");
//       });
//     }
//   }
// });
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(29)))

/***/ }),

/***/ 36:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
module.exports=function(){return{restrict:'E',template:'<div/>',scope:{height:'='},link:function link(scope,elm,attr){elm[0].querySelector('div').style.height=scope.height+'px';}};};

/***/ }),

/***/ 37:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
module.exports=function($q,imageResize,$compile,$timeout){return{restrict:'A',transclude:true,scope:{process:'=?',upimage:'&'},template:'\n  <ng-transclude></ng-transclude>\n  <input type="file" style="display:none;">\n  ',link:function link(scope,elm,attr){scope.process=false;var upImage=function upImage(image){scope.process=false;scope.upimage({data:image});};var loadImage=function loadImage(flag){$timeout(function(){scope.process=flag?true:false;});};elm[0].addEventListener('click',function(){return elm[0].querySelector('input[type="file"]').click();});elm[0].querySelector('input[type="file"]').addEventListener('change',function(changeEvent){loadImage(true);var reader=new FileReader();reader.onload=function(event){var src=event.target.result;if(!src.match(/^data\:(.*?)\//))return loadImage(false);if(src.match(/^data\:(.*?)\//)[1]!=='image')return loadImage(false);var image=new Image();image.src=src;image.onload=function(){return upImage(imageResize(image,1500));};loadImage(false);};reader.readAsDataURL(changeEvent.target.files[0]);elm[0].querySelector('input[type="file"]').value='';});}};};

/***/ }),

/***/ 38:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
module.exports=function($q){var imageResize;imageResize=function imageResize(img,size){var canvas,ctx,h,temp,w;w=img.width;h=img.height;canvas=document.createElement('canvas');ctx=canvas.getContext('2d');temp=Math.sqrt(img.width*img.width+img.height*img.height);if(temp>size){w=size*img.width/temp;h=size*img.height/temp;}canvas.width=w;canvas.height=h;ctx.drawImage(img,0,0,w,h);return canvas.toDataURL();};return imageResize;};

/***/ })

},[300]);